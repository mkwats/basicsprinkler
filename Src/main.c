
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f3xx_hal.h"
#include "dma.h"
#include "i2c.h"
#include "rtc.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "Serial.h"
#include "PacketProcess.h"  //one for this project only



/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

#define APP_WELCOME "\nBasicSprinkler\n"

char SerialMsgData[100];
TMsg msg = {.Len=0, .Data= &SerialMsgData[0]};

/*UART*/
//HC05/GPS 			UART1
//Virtual(usb) 		UART2
//UBLOX 			UART3
//ESP 				UART4

typedef enum ParseUnit{
	WROOM,
	HC05,
	VIRTUAL} ParseUnit;



	//create the serial circular buffer
	uint8_t serial_virtual_rx_buffer[60];
	Serial_t serial_virtual = {
			.buffer = serial_virtual_rx_buffer,
			.head =0,
			.tail = 0,
			.size = 60,
			.huart = &huart3  //now HC-05
	};

	//create a parser
	uint8_t packet_buffer[50];
	PP_HandleTypeDef virtualParser = {
			.timeout = 100,
			.numCharToRead=50,
			.packet = packet_buffer,
			.packetMaxLen =50,
			.hSerial = &serial_virtual
	};

uint8_t solenoidActive = 0;
uint16_t countDownTime = 0;
uint16_t countDownLitres = 0;
uint16_t  usedLitres, usedTime=0;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

void User_Parse_Init(ParseUnit p);
void User_Parse_Read(ParseUnit p);

void User_Parse_NVIC_reset(PP_Packet_t *packet);
void User_Parse_SetDate(PP_Packet_t *packet);
void User_Parse_PrintTime(PP_Packet_t *packet);
void User_Parse_Test(PP_Packet_t *packet);
void User_Parse_Unit_Test(void);
uint32_t User_get_long(PP_Packet_t *pkt);

void User_Solenoid_Execute(PP_Packet_t *packet);
void User_Solenoid_Minutes(PP_Packet_t *packet);
void User_Solenoid_Litres(PP_Packet_t *packet);
void User_Solenoid_Stop_Active(PP_Packet_t *packet);
void User_Solenoid_Status(PP_Packet_t *packet);

void User_Solenoid_Stop(uint8_t solenoid);
void User_Solenoid_Start(uint8_t solenoid);


/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();
  MX_I2C1_Init();
  MX_TIM15_Init();
  MX_RTC_Init();
  MX_USART3_UART_Init();
  MX_TIM16_Init();
  /* USER CODE BEGIN 2 */

  msg.Len = sprintf(msg.Data,"Basic Sprinkler\n\n");
  User_Common_SendData(msg);

  //test the parser
 //User_Parse_Unit_Test();

  /************/
  //Initialise the parser
    User_Parse_Init(VIRTUAL);
  //  User_Parse_Init(HC05);

#ifdef ff
    /*##-4- Start the Input Capture in interrupt mode ##########################*/
    if (HAL_TIM_IC_Start(&htim15, TIM_CHANNEL_2) != HAL_OK)
    {
      /* Starting Error */
      Error_Handler();
    }

    /*##-5- Start the Input Capture in interrupt mode ##########################*/
    if (HAL_TIM_IC_Start(&htim15, TIM_CHANNEL_1) != HAL_OK)
    {
      /* Starting Error */
      Error_Handler();
    }
#endif

    HAL_TIM_OC_Start_IT(&htim16, TIM_CHANNEL_1);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  User_Parse_Read(VIRTUAL);
	// User_Parse_Read(HC05);
	  if(solenoidActive != 0 && (countDownTime == 0 || countDownLitres == 0 )){
		  User_Solenoid_Stop(solenoidActive);
	  }
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Configure LSE Drive Capability 
    */
  HAL_PWR_EnableBkUpAccess();

  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_I2C1|RCC_PERIPHCLK_RTC
                              |RCC_PERIPHCLK_TIM15|RCC_PERIPHCLK_TIM16;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  PeriphClkInit.Tim15ClockSelection = RCC_TIM15CLK_HCLK;
  PeriphClkInit.Tim16ClockSelection = RCC_TIM16CLK_HCLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

void User_Common_SendData(TMsg msg){
	//sned to virtual
	HAL_UART_Transmit(&huart2, (uint8_t*)msg.Data, msg.Len,1000);
	//send to Bluetooth
	HAL_UART_Transmit(&huart3,  (uint8_t*)msg.Data, msg.Len,1000);
}

uint32_t User_get_long(PP_Packet_t *pkt) {

	if(1){
		//method 1
		char	*cursor;
		//(ctx->packet+ctx->packetCursorPos) ===  &ctx->packet[ctx->packetCursorPos]
		uint32_t ret =	strtoul((char*)&pkt->packet[pkt->packetCursorPos], &cursor,10);
		pkt->packetCursorPos = (uint8_t*)cursor - (uint8_t*)pkt->packet; //this might need inspection
		return ret;
	}


}

#define N_DECIMAL_POINTS_PRECISION (100) // n = 3. Three decimal points.
#define TICK_TIME 8000/81 //msec
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim){
	static char data[50];

	//timer_tick_frequency = Timer_default_frequency / (prescaller_set + 1)
//   = 8000000/(80+1) // htim3.Init.Prescaler = 80
	// time = 1/f
	if(htim->Instance == TIM15){// time from ch1 to ch4
			uint16_t a = (HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1));
			uint16_t b = (HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_2));
			uint8_t len = sprintf(&data[0],"%i---%i---%li---%i \n",a, b, __HAL_TIM_GET_COUNTER(htim), 1);
			HAL_UART_Transmit(&huart2,(uint8_t*)&data,len,1000);
		}

}

void HAL_TIM_OC_DelayElapsedCallback(TIM_HandleTypeDef *htim){
	if(htim->Instance == TIM16){
		if(countDownTime>0)countDownTime--;
	//	msg.Len = sprintf(msg.Data,"%li\n",HAL_GetTick());
	//	User_Common_SendData(msg);
		if(solenoidActive){
			usedTime++;
		}
		HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
	}
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
	static uint8_t PB_counter =0 ; 			//push button counter for cycle EXTI
	static uint32_t IntTimeLastPIN13 = 0;
	uint32_t 	IntTimeNow;

	//water meter pulse
	if (GPIO_Pin == GPIO_PIN_4){
		if(countDownLitres >0) countDownLitres--;
		if(solenoidActive){
			usedLitres++;
		}
	}

	//nucleo push button
	if (GPIO_Pin == GPIO_PIN_13){
		//set the time now for software debounce
		IntTimeNow = HAL_GetTick();

		/* Manage software debouncing*/
		/* If I receive a button interrupt after more than 300 ms from the first one I get it, otherwise I discard it */
		if ((IntTimeNow - IntTimeLastPIN13) > 300U)
		{
			IntTimeLastPIN13 = IntTimeNow;  //reset the last event time
			switch (PB_counter) {
				case 0:
					PB_counter ++;
					break;

				case 1:
					PB_counter = 0;
					break;

				default:
					break;
			}
		}
	}


}


/* Start then DMA recieve
 * Add relationship between packets received and function to call
 * * * * * * *
 * DMA needs to be setup with circular buffer
 * this function will start the DMA receive and load the characters that
 * will be searched for when in the main loop the serial buffer (filled by the DMA)
 * is inspected.
 * The buffer is overwritten if not inspected.
 */
void User_Parse_Init(ParseUnit p){


	switch (p) {
		case VIRTUAL:
			Serial_init_dma(&serial_virtual);
			  PP_item_alloc((char*)"AA",NULL,&User_Solenoid_Minutes,&virtualParser); //test the function of the parse.
			  PP_item_alloc((char*)"AB",NULL,&User_Solenoid_Litres,&virtualParser); //test the function of the parse.
			  PP_item_alloc((char*)"AC",NULL,&User_Solenoid_Stop_Active,&virtualParser); //test the function of the parse.
			  PP_item_alloc((char*)"AD",NULL,&User_Solenoid_Status,&virtualParser); //test the function of the parse.
			  break;

		case WROOM:

			  break;

		case HC05:

			break;

		default:
			break;
	}
}


/*Function to call to read the serial and work out what to do with it*/
void User_Parse_Read(ParseUnit p){
	uint8_t sa = Serial_available(&serial_virtual);
	switch (p) {
		case VIRTUAL:
			if(sa ){
				virtualParser.numCharToRead=50;  //reset the counter
				PP_Status_t ret = PP_packet_get(&virtualParser);//defaulted to 50 characters, needs considering
				if(ret==PP_PACKET_FOUND ){
				  PP_proces_packet_simple(&virtualParser);
				}
			}
			break;
		case WROOM:

			break;
		case HC05:

			break;
		default:
			break;
	}


}




void User_Parse_NVIC_reset(PP_Packet_t *packet){
	NVIC_SystemReset();
}

void User_Parse_SetDate(PP_Packet_t *packet){

}

void User_Parse_Test(PP_Packet_t *packet){

}



void User_Parse_Unit_Test(void){

	//create the serial circular buffer
	uint8_t serial_virtual_rx_buffer[60];
	Serial_t serial_virtual = {
			.buffer = serial_virtual_rx_buffer,
			.tail = 60,
			.size = 60,
			.huart = &huart2
	};

	//initialise the serial buffer
	Serial_init_dma(&serial_virtual);

	//create a parser
	uint8_t packet_buffer[50];
	PP_HandleTypeDef MyParser = {
			.timeout = 100,
			.numCharToRead=50,
			.packet = packet_buffer,
			.hSerial = &serial_virtual
	};

	//allocate items to the parser
	PP_item_alloc((char*)"AA",NULL,&User_Parse_Test,&MyParser); //test the function of the parse.

	//test with a good packet
    msg.Len = sprintf(msg.Data,"\n\n----------\nProcessPacket: ");
    User_Common_SendData(msg);

    /*
     * put a packet into Parser, Later can use Serial to find packet
     * this emulates packet_get result
     */
    //copy the packet to the parser
	strcpy((char*)MyParser.packet,"SNMATT1234:II1235:AA1,10,JOB_DONE");
	//Output the packet
	msg.Len = sprintf(msg.Data,"%s", MyParser.packet); User_Common_SendData(msg);
	//Parser needs the packet length
	MyParser.packetLen = msg.Len;
	//Parser to process the packet
	PP_Status_t res = PP_proces_packet(&MyParser);

	//Output the result
	msg.Len = sprintf(msg.Data,"ProcessPacket result: %i \n----------\n", res); User_Common_SendData(msg);


	//test with a good packet Simple
    msg.Len = sprintf(msg.Data,"\n\n----------\nPP_process_packet_simple: ");
    User_Common_SendData(msg);

    //copy the packet to the parser
	strcpy((char*)MyParser.packet,"AA1,10,JOB_DONE");
	//Output the packet
	msg.Len = sprintf(msg.Data,"%s", MyParser.packet); User_Common_SendData(msg);
	//Parser needs the packet length
	MyParser.packetLen = msg.Len;
	//Parser to process the packet
	res = PP_proces_packet_simple(&MyParser);

	//Output the result
	msg.Len = sprintf(msg.Data,"PP_process_packet_simple result: %i \n----------\n", res); User_Common_SendData(msg);



#ifdef ff
	 SerialPause();

	  //not long enough
	  Serial.print(F("\n\n----------\nProcessPacket: "));
	  char packet[50] = "S";
	  Serial.print(packet);
	  res=MyParser.ProcessPacket(packet, strlen(packet));
	  Serial.print(F("result :"));
	  Serial.println(res);
	  Serial.println(F("----------"));

	  SerialPause();

	  //long enough but no SN
	  Serial.print(F("\n\n----------\nProcessPacket: "));
	  strcpy(packet,"SMAAAAAAAAA");
	  Serial.print(packet);
	  res=MyParser.ProcessPacket(packet, strlen(packet));
	  Serial.print(F("result :"));
	  Serial.println(res);
	  Serial.println(F("----------"));

	  SerialPause();

	  //wrong SN
	  Serial.print(F("\n\n----------\nProcessPacket: "));
	  strcpy(packet,"SNATT1234:II1234:AA1234");
	  Serial.print(packet);
	  res=MyParser.ProcessPacket(packet, strlen(packet));
	  Serial.print(F("result :"));
	  Serial.println(res);
	  Serial.println(F("----------"));

	  SerialPause();

	  //no II
	  Serial.print(F("\n\n----------\nProcessPacket: "));
	  strcpy(packet,"SNMATT1234:IA1234:AA1234");
	  Serial.print(packet);
	  res=MyParser.ProcessPacket(packet, strlen(packet));
	  Serial.print(F("result :"));
	  Serial.println(res);
	  Serial.println(F("----------"));

	  SerialPause();

	  //no ii
	  Serial.print(F("\n\n----------\nProcessPacket: "));
	  strcpy(packet,"SNMATT1234:I");
	  Serial.print(packet);
	  res=MyParser.ProcessPacket(packet, strlen(packet));
	  Serial.print(F("result :"));
	  Serial.println(res);
	  Serial.println(F("----------"));

	  SerialPause();

	  //ii but no number
	  Serial.print(F("\n\n----------\nProcessPacket: "));
	  strcpy(packet,"SNMATT1234:II");
	  Serial.print(packet);
	  res=MyParser.ProcessPacket(packet, strlen(packet));
	  Serial.print(F("result :"));
	  Serial.println(res);
	  Serial.println(F("----------"));

	  SerialPause();

	  //no instruction
	  Serial.print(F("\n\n----------\nProcessPacket: "));
	  strcpy(packet,"SNMATT1234:II1234");
	  Serial.print(packet);
	  res=MyParser.ProcessPacket(packet, strlen(packet));
	  Serial.print(F("result :"));
	  Serial.println(res);
	  Serial.println(F("----------"));

	  SerialPause();

	  //an instruciton but not match af
	  Serial.print(F("\n\n----------\nProcessPacket: "));
	  strcpy(packet,"SNMATT1234:II1234:AF1234,1233");
	  Serial.print(packet);
	  res=MyParser.ProcessPacket(packet, strlen(packet));
	  Serial.print(F("result :"));
	  Serial.println(res);
	  Serial.println(F("----------"));

	  SerialPause();

	  //all good.
	  Serial.print(F("\n\n----------\nProcessPacket: "));
	  strcpy(packet,"SNMATT1234:II1234:AB1234,12322,JOB_DONE");
	  Serial.print(packet);
	  res=MyParser.ProcessPacket(packet, strlen(packet));
	  Serial.print(F("result :"));
	  Serial.println(res);
	  Serial.println(F("----------"));
#endif


}

void User_Parse_PrintTime(PP_Packet_t *packet){
    /* Display the updated Time and Date */
	uint8_t aShowTime[50] = {0};
	uint8_t aShowDate[50] = {0};
    RTC_CalendarShow(aShowTime, aShowDate,&hrtc);

    msg.Len = sprintf(msg.Data,"The current time: %s", aShowTime);
    User_Common_SendData(msg);
}

void User_Solenoid_Execute(PP_Packet_t *packet){

}

void User_Solenoid_Minutes(PP_Packet_t *packet){
	msg.Len = sprintf(msg.Data,"\tIn User_Solenoid_Minutes: %s\n", packet->packet);
	    User_Common_SendData(msg);
	//expect AA1:2334
	//1 is the solenoid number
	//2334 is the number of ticks to count before stopping.

	if(solenoidActive){
		//bail
		msg.Len = sprintf(msg.Data,"\tSoleoid already active: %i\n", solenoidActive);
		User_Common_SendData(msg);
		return;
	}


	packet->packetCursorPos = 2; //skip the AA
	uint8_t solenoid =  (uint8_t)User_get_long(packet);
    msg.Len = sprintf(msg.Data,"\t\tSolenoid Number: %i\n",solenoid);
    User_Common_SendData(msg);

    //check that have ,

    if (!strcmp(packet->packet + packet->packetCursorPos, ",")){
        msg.Len = sprintf(msg.Data,"\t\t%s\n","Failed no ','"); 	User_Common_SendData(msg);
	} else {
		packet->packetCursorPos++ ; //skip the ,
		countDownTime =  (uint16_t)User_get_long(packet);
		msg.Len = sprintf(msg.Data,"\t\tNumber of Ticks: %i\n",countDownTime);
		User_Common_SendData(msg);
	}

    //turn on the solenoid
	countDownLitres = 1000; // set a defualt just in case.
    User_Solenoid_Start(solenoid);



    //need to add this to a controller or task that is monitored...  complex solution
    //or just a count down that in check on the pulse interupt, or main loop that is run every second?... simple solution
}


void User_Solenoid_Litres(PP_Packet_t *packet){
	msg.Len = sprintf(msg.Data,"\tIn User_Solenoid_Litres: %s\n", packet->packet);
	    User_Common_SendData(msg);
	//expect AA1:2334
	//1 is the solenoid number
	//2334 is the number of ticks to count before stopping.

	if(solenoidActive){
		//bail
		msg.Len = sprintf(msg.Data,"\tSoleoid already active: %i\n", solenoidActive);
		User_Common_SendData(msg);
		return;
	}


	packet->packetCursorPos = 2; //skip the AB
	uint8_t solenoid =  (uint8_t)User_get_long(packet);
    msg.Len = sprintf(msg.Data,"\t\tSolenoid Number: %i\n",solenoid);
    User_Common_SendData(msg);

    //check that have ,

    if (!strcmp(packet->packet + packet->packetCursorPos, ",")){
        msg.Len = sprintf(msg.Data,"\t\t%s\n","Failed no ','"); 	User_Common_SendData(msg);
	} else {
		packet->packetCursorPos++ ; //skip the ,
		countDownLitres =  (uint16_t)User_get_long(packet);
		msg.Len = sprintf(msg.Data,"\t\tNumber of Ticks: %i\n",countDownTime);
		User_Common_SendData(msg);
	}

    //turn on the solenoid
	countDownTime = 60*60; //60minutes // set a defualt just in case.
    User_Solenoid_Start(solenoid);



    //need to add this to a controller or task that is monitored...  complex solution
    //or just a count down that in check on the pulse interupt, or main loop that is run every second?... simple solution
}

void User_Solenoid_Status(PP_Packet_t *packet){
	msg.Len = sprintf(msg.Data,"S%u active:\n \tLitres %i\n\tTime: %i", solenoidActive, usedLitres, usedTime);
	User_Common_SendData(msg);
}

void User_Solenoid_Stop_Active(PP_Packet_t *packet){
	User_Solenoid_Stop(solenoidActive);
}

void User_Solenoid_Stop(uint8_t solenoid){
	uint8_t aSolenoidActive = solenoidActive;

	switch (solenoidActive) {
		case 1:
			solenoidActive = 0;
			countDownLitres = 0;
			countDownTime=0;
			HAL_GPIO_WritePin(SOLENOID1_GPIO_Port, SOLENOID1_Pin,GPIO_PIN_RESET);
			break;
		case 2:
			solenoidActive = 0;
			countDownLitres = 0;
			countDownTime=0;
			HAL_GPIO_WritePin(SOLENOID2_GPIO_Port, SOLENOID2_Pin,GPIO_PIN_RESET);
			break;
		default:
			break;
	}
	msg.Len = sprintf(msg.Data,"S%u stopped:\n \tLitres %i\n\tTime: %i", aSolenoidActive, usedLitres, usedTime);
	User_Common_SendData(msg);
}


void User_Solenoid_Start(uint8_t solenoid){
	switch (solenoid) {
		case 1:
			solenoidActive =1;
			HAL_GPIO_WritePin(SOLENOID1_GPIO_Port, SOLENOID1_Pin,GPIO_PIN_SET);
			break;
		case 2:
			solenoidActive = 2;
			HAL_GPIO_WritePin(SOLENOID2_GPIO_Port, SOLENOID2_Pin,GPIO_PIN_SET);
			break;
		default:
			break;
	}
	usedTime=0;
	usedLitres=0;

	msg.Len = sprintf(msg.Data,"S%u started:\n \tLitres %i\n\tTime: %i", solenoid, countDownLitres, countDownTime);
	User_Common_SendData(msg);
}


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

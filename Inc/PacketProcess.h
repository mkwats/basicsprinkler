/*
 * PacketProcess.h
 *
 *  Created on: 31Dec.,2018
 *      Author: mkwat
 */
/*this one is for the Basic Sprinkler*/

#ifndef PACKETPROCESS_H_
#define PACKETPROCESS_H_


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifdef STM32F302x8
#include "stm32f3xx_hal.h"
#endif

#include "Serial.h" //this is the serial packet handler.
#include "stdint.h"
#include "stdlib.h"
#include "ctype.h"
#include "string.h"

//serial debugger.
extern TMsg msg;
extern void User_Common_SendData(TMsg msg);
static void serial_print(char *data){
	msg.Len = sprintf(msg.Data,"%s",data);
	User_Common_SendData(msg);
}

static void serial_println(char *data){
	msg.Len = sprintf(msg.Data,"%s\n",data);
	User_Common_SendData(msg);
}

//Packet definition
#define SOP  '{'
#define EOP  '}'
#define PARSE_NUMBER_OF_ITEMS 10
//uncomment if want to look for II123: at the start of packet.. used to provide response context
//#define PARSE_LOOK_FOR_II

typedef enum {
	SOP_search,
	SOP_found,
	EOP_found //now acting on it
} PP_SeekState;


typedef enum {
	PP_OK,
	PP_PACKET_FOUND,
	PP_STREAM_TIMEOUT,
	PP_END_CONTENT,
	PP_NO_BUFFER,
	PP_NO_STREAM,
	PP_ERROR_PARSE,
	PP_ERROR_II_NO_CONTENT,
	PP_ERROR_II_NO_II,
	PP_ERROR_II_NO_NUMBER,
	  Pa_SERIAL_NO_CONTENT,//1
	  Pa_SERIAL_NO_SN,//2
	  Pa_SERIAL_INCORRECT,  //3
	  Pa_II_NO_II, //4
	  Pa_II_NO_CONTENT, //5
	  Pa_II_NO_NUMBER,//6
	  Pa_ITEM_NO_CONTENT,  //7
	  Pa_ITEM_NOT_FOUND //8
} PP_Status_t;

/*typedef foe the broken down packet*/
typedef struct PP_Packet_t{
  char 			SN[9];  //store the SN
  char 			ID[3]; //function to call
  uint32_t 		II;  //store the Instruction ID
  char* 		packet;  //pointer to the packet
  uint8_t 		packetLength;    //lenght of the paceket
  uint8_t 		packetCursorPos;   //the current position reading from 0 to packetLength-1
  uint8_t 		packetMaxLen;   //do don't overrun the packet buffer
}PP_Packet_t;


typedef void(*parse_match_fn)(PP_Packet_t*);//(void * hint);

//can forward define but then loose content on intelityep
//typedef struct PP_ParseItem_t PP_ParseItem_t;

/* this is the definition of the parse item
the PP will look at the packet for a match with the iD
if there is one will call execute
the schema, description and help test are info that can be sent to an output*/
typedef struct PP_ParseItem_t {
	char						Id[3];  //need space for \0
	parse_match_fn      		execute;
	void						*hint;  //could be a struct instead of using the char( and uint8_t as is now.  pointer to a packet type
	char						*Schema;          //{c%0%2%set a version%Will set if }"
	char						*Description;     //F("Set: EEPROM VERSION, will force EEPROM to default settings"),
	char						*HelpText;       //F("Type ss then , to get what you want"),
	struct PP_ParseItem_t		*next;
	struct PP_HandleTypeDef		*hSeek;
}PP_ParseItem_t ;



/* type def for the PacketProcessor*/
typedef struct PP_HandleTypeDef{
	Serial_t			*hSerial;  //pointer to the serial stream to read

	uint8_t				numCharToRead; //number of character to read, packet search stops on 0 or timeout
	uint16_t			timeout; //time to wait for serial characters

	//pacekt found info... change to a struct... then can pass to other functions ie execute in the item
	//uint8_t				*buffer;  //place to put the packet found
//	uint8_t				bufferLen;  //max lenght of packet

	uint8_t				*packet;  //place to put the packet found
	uint8_t				packetLen; //the lenght of found packet, if 0 no packet
	uint8_t 			packetMaxLen;
	uint8_t				packetCursorPos;   //the current position reading from 0 to packetLength-1,  used when parsing
	PP_Packet_t			*aPacket;  //can have seperate packet  pointer (or use the below) TBC

	//link to parse list
	PP_ParseItem_t		*root;  //link to the parse item list



	char 				SN[9];  //store the SN
	char 				ID[3]; //function to call (confusing with II need to new name)
	uint32_t 			II;  //store the Instruction ID
}  PP_HandleTypeDef;



PP_ParseItem_t * 			PP_item_alloc(char* aId, void * hint, parse_match_fn task_fn, PP_HandleTypeDef *hSeek);
PP_Status_t 				PP_proces_packet(PP_HandleTypeDef *ctx);
PP_Status_t 				PP_proces_packet_simple(PP_HandleTypeDef *ctx);
PP_Status_t 				PP_proces_packet_2(PP_HandleTypeDef *ctx); //the old one..
PP_Status_t 				PP_packet_get(PP_HandleTypeDef * pSeek);

void 						PP_item_print_help(void *p);

/*
static PP_ParseItem_t * 	get_last_linked_item(PP_HandleTypeDef * ctx);
static void 				link_item(PP_HandleTypeDef * ctx,PP_ParseItem_t * task);
static PP_Status_t 			instruction_id_get(PP_HandleTypeDef *pkt);
static uint32_t 			get_long(PP_HandleTypeDef *ctx);

static PP_Status_t         	serial_number_get(PP_HandleTypeDef *ctx, char* serial);
static PP_Status_t        	instruction_id_get(PP_HandleTypeDef *ctx);
static PP_Status_t        	instruction_get(PP_HandleTypeDef *ctx);
static PP_Status_t 			item_call(PP_HandleTypeDef *ctx);
*/

static PP_ParseItem_t * get_last_linked_item(PP_HandleTypeDef * ctx)
{
	PP_ParseItem_t * last;
	for (last = ctx->root; NULL != last; last = last->next) {
		if (NULL == last->next) {
			break;
		}
	}
	return last;
}

static void link_item(PP_HandleTypeDef * ctx,PP_ParseItem_t * task)
{
	task->hSeek = ctx;
	task->next = NULL;

	if (NULL == ctx->root) {
		ctx->root = task;
	} else {
		PP_ParseItem_t * last = get_last_linked_item(ctx);
		last->next = task;
	}
}

static uint32_t get_long(PP_HandleTypeDef *ctx) {

	if(1){
		//method 1
		char	*cursor;
		//(ctx->packet+ctx->packetCursorPos) ===  &ctx->packet[ctx->packetCursorPos]
		uint32_t ret =	strtoul((char*)&ctx->packet[ctx->packetCursorPos], &cursor,10);
		ctx->packetCursorPos = (uint8_t*)cursor - (uint8_t*)ctx->packet; //this might need inspection
		return ret;
	}
	else{
		//method2
	  char ch[16] = {'\0'};
	  uint8_t j = 0;
	  uint8_t pos = ctx->packetCursorPos;

	  for (uint8_t i = 0; i < 16; i++) {
		if (isdigit(ctx->packet[pos])) {
		  ch[j++] = ctx->packet[pos];
		} else {
		  ch[j] = '\0';
		  ctx->packetCursorPos = ctx->packetCursorPos + j;
		  break;
		}
	  }
	  return atol(ch);
	}

}

static PP_Status_t instruction_id_get(PP_HandleTypeDef *pkt) {
//PaRESULT Parser::InstructionIdGet(Packet_t *pkt) {
	PP_Status_t pa = PP_OK;
  //PaRESULT pa = Pa_OK;
  uint8_t i = pkt->packetCursorPos;

  if (i + 2 > pkt->packetLen){
      pa = PP_ERROR_II_NO_CONTENT;
      return pa;
  }

  if (pkt->packet[i++] !=  'I' || pkt->packet[i++] != 'I'){
    pa = PP_ERROR_II_NO_II;
    return pa;
  }

  pkt->packetCursorPos = i;

  pkt->II = (uint32_t)get_long(pkt);
  if (pkt->II == 0){
    pa = PP_ERROR_II_NO_NUMBER;
    return pa;
  }

  return pa;
}

static PP_Status_t serial_number_get(PP_HandleTypeDef *pkt, char* serial) {
//PaRESULT Parser::SerialNumberGet(Packet_t *pkt, char* serial) {
	PP_Status_t pa = PP_OK;
  uint8_t i = pkt->packetCursorPos;
  uint8_t SNlength=0;
  while(serial[SNlength]!='\0'){SNlength++;};

  if(pkt->packetLen < 2 + SNlength){
    pa = Pa_SERIAL_NO_CONTENT;
    return pa;
  }

  if (pkt->packet[i++] != 'S'  || pkt->packet[i++] != 'N'){
    pa = Pa_SERIAL_NO_SN;
    return pa;
  }

  for(uint8_t k =0; k < SNlength; k++){
    if (pkt->packet[i] != serial[k]) {
      pa = Pa_SERIAL_INCORRECT;
      return pa;
    }
    pkt->SN[k]=pkt->packet[i];  //store SN in pkt
    i++;
  }

  pkt->packetCursorPos = i;
  return pa;
}

static PP_Status_t instruction_get(PP_HandleTypeDef *pkt) {
//PaRESULT Parser::InstructionGet(Packet_t *pkt) {
	PP_Status_t pa = PP_OK;
  uint8_t i = pkt->packetCursorPos;

  if (i + 2 > pkt->packetLen){
    pa = Pa_ITEM_NO_CONTENT;
    return pa;
  }

  pkt->ID[0]=pkt->packet[i];
  pkt->ID[1]=pkt->packet[i+1];
  pkt->ID[2]='\0';

  return pa;
}


static PP_Status_t item_call(PP_HandleTypeDef *ctx) {
//PaRESULT Parser::ItemCall(char* Id, char* packet, uint8_t packetLength){
	PP_Status_t pa = Pa_ITEM_NOT_FOUND;

	PP_ParseItem_t * task;
	for (task = ctx->root; NULL != task; task = task->next) {
		if (ctx->ID[0] == task->Id[0] && ctx->ID[1] == task->Id[1]){
			//task->execute(task->hint);
			PP_Packet_t pkt;
			pkt.packet =(char*)&ctx->packet[ctx->packetCursorPos];
			pkt.packetLength = (uint8_t)(ctx->packetLen - ctx->packetCursorPos);
			pkt.packetCursorPos = 0;  //ctx->packetCursorPos;  as have move the pointer to the start

			//task->execute((char*)&ctx->packet[ctx->packetCursorPos], (uint8_t)(ctx->packetLen - ctx->packetCursorPos));
			task->execute(&pkt);
			pa = PP_OK;
			break;
		}
	}
  return pa;
}


PP_ParseItem_t * PP_item_alloc(char* aId,void * hint,parse_match_fn task_fn,PP_HandleTypeDef *hSeek)
{
	PP_ParseItem_t * task = NULL;

	task = (PP_ParseItem_t *) malloc(sizeof(PP_ParseItem_t));
	if (NULL == task) {
		goto out;
	}

	task->next = NULL;
	task->execute = task_fn;
	task->hint = hint;   //not used at the moment, but might reconsider

	task->Id[0] = aId[0];
	task->Id[1] = aId[1];
	task->Id[2] = '\0';

	task->HelpText = NULL;
	task->Schema = NULL;
	task->Description = NULL;

	task->hSeek = hSeek;

	link_item(hSeek, task);

out:
	return task;
}


PP_Status_t PP_packet_get(PP_HandleTypeDef * pSeek) {
	if (pSeek->packet == NULL) return PP_NO_BUFFER;
	if (pSeek->hSerial == NULL) return PP_NO_STREAM;

	//clear the packet
	pSeek->packet[0] = '\0';
	pSeek->packetLen = 0;
	pSeek->packetCursorPos = 0;
	uint8_t n = 0;
	PP_SeekState searchState = SOP_search;
	uint32_t t = HAL_GetTick();

	while (HAL_GetTick() - t < pSeek->timeout && pSeek->numCharToRead > 0) {
		if (Serial_available(pSeek->hSerial)) {
			char c = Serial_get(pSeek->hSerial);
			pSeek->numCharToRead--;

			t = HAL_GetTick(); //reset counter
			switch (c) {
			case '\n': case '\r':
				break; // so can read another
			case SOP:
				if (searchState != SOP_search) {
					//found a cSOP at the wrong time
					break;
				} else {
					//SOP found...
					searchState = SOP_found;
					break;
				}
			case EOP:
				if (searchState != SOP_found) {
					//EOP found... but not SOP so discarding it!
					break;
				} else {
					//found cEOP do something with it
					//add null terminator
					pSeek->packet[n] = '\0';
					pSeek->packetLen = n;
					return PP_PACKET_FOUND;
				}
			default:
				if (searchState != SOP_found) break;
				//do stuff now we have SOP.
				// put he char in string
				if (n < pSeek->packetMaxLen - 1) { //buffer size -1 for null char
					pSeek->packet[n] = c;
					n++;
				} else {
					//buffer full: clear the buffer and start again
					pSeek->packet[0] = '\0';
					n = 0;
					searchState = SOP_search;
				}
			}//end switch
		} //while (s->available() > 0)
	} //timeout while

	PP_Status_t ret = PP_OK;
	if (pSeek->numCharToRead == 0) {
		ret = PP_END_CONTENT;
	} else {
		ret = PP_STREAM_TIMEOUT;
	}

	//only return from EOP if get her means no packet
	//clear the buffer
	pSeek->packet[0] = '\0';
	return ret;
}


PP_Status_t PP_proces_packet_simple(PP_HandleTypeDef *ctx){
	  PP_Status_t pa = PP_OK;
	  ctx->packetCursorPos =0;

	 serial_println("\n******\nLooking for instruction");
	  pa = instruction_get(ctx);
	  if(pa==PP_OK){
	      msg.Len = sprintf(msg.Data,"\tinstruction_get found: \'%s\'", ctx->ID);
	      User_Common_SendData(msg);
	  }else{
	      msg.Len = sprintf(msg.Data,"\tinstruction_get failed with code: %i\n", pa);
	      User_Common_SendData(msg);
	      goto bail;
	      //return pa;
	  }

	  serial_println("\n******\nCalling Instruction");
	  pa = item_call(ctx); //pass the packet current location.
	  if(pa!=PP_OK){
	      serial_print("\titem_call failed with code: ");
	      msg.Len = sprintf(msg.Data,"%i\n", pa); User_Common_SendData(msg);
	      goto bail;
	      //return pa;
	  }

	bail:
	  serial_println("\n\n******\nProcess Packet Complete \n*****************\n");
	  return pa;

}


//pkt - pointer to the packet that will be parsing.
//return
/*this is the one that for {SNMATT1234:II1234:AA1234}*/
PP_Status_t PP_proces_packet(PP_HandleTypeDef *ctx) {
  PP_Status_t pa = PP_OK;

 // Packet_t myPacket; //use the passed ctx.Packet
 // myPacket.packet = pkt;
 // myPacket.packetLength = pktLen;
ctx->packetCursorPos =0;
  //memset(myPacket.SN,'\0',9);
	memset(ctx->SN,'\0',9); //set the SN to nothing

	serial_print("\n\n*****************\nProcess Packet Begin \n******\n");
	serial_print("Packet processing: ");
	msg.Len = sprintf(msg.Data,"%s\n", ctx->packet); User_Common_SendData(msg);

	serial_println("\n******\nLooking for Serial number ");
	pa = serial_number_get(ctx, (char*)"MATT1234\0");
	if(pa==PP_OK){
		serial_print("\tSerial found and matched: ");
		msg.Len = sprintf(msg.Data,"%s\n", ctx->SN); User_Common_SendData(msg);
		serial_print("\tCurrent Cursor Location: ");
		msg.Len = sprintf(msg.Data,"%i\n", ctx->packetCursorPos); User_Common_SendData(msg);
		serial_print("\tChar at this Location: \'");
	      msg.Len = sprintf(msg.Data,"%c", ctx->packet[ctx->packetCursorPos]); User_Common_SendData(msg);
		serial_println("\'");
	}else{
		serial_print("\nSerial get failed with code: ");
		msg.Len = sprintf(msg.Data,"%i\n", pa); User_Common_SendData(msg);
	      goto bail;
		//return pa;
	}

  //should be at a : so skip it
  ctx->packetCursorPos++;
  serial_println("\n******\nLooking for instruction id");
  pa = instruction_id_get(ctx);
  if(pa==PP_OK){
      serial_print("\tinstruction_id_get found and matched: ");
      msg.Len = sprintf(msg.Data,"%li\n", ctx->II); User_Common_SendData(msg);
      serial_print("\tCurrent Cursor Location: ");
      msg.Len = sprintf(msg.Data,"%i\n", ctx->packetCursorPos); User_Common_SendData(msg);
      serial_print("\tChar at this Location: \'");
      msg.Len = sprintf(msg.Data,"%c", ctx->packet[ctx->packetCursorPos]); User_Common_SendData(msg);
      serial_println("\'");
  }else{
      serial_print("\ninstruction_id_get failed with code: ");
      msg.Len = sprintf(msg.Data,"%i\n", pa); User_Common_SendData(msg);
      goto bail;
      //return pa;
  }

   //should be at a : so skip it
  ctx->packetCursorPos++;
  serial_println("\n******\nLooking for instruction");
  pa = instruction_get(ctx);
  if(pa==PP_OK){
      serial_print("\tinstruction_get found: \'");
      msg.Len = sprintf(msg.Data,"%s", ctx->ID); User_Common_SendData(msg);
      serial_println("\'");
  }else{
      serial_print("\tinstruction_get failed with code: ");
      msg.Len = sprintf(msg.Data,"%i\n", pa); User_Common_SendData(msg);
      goto bail;
      //return pa;
  }

  serial_println("\n******\nCalling Instruction");
  pa = item_call(ctx); //pass the packet current location.
  if(pa!=PP_OK){
      serial_print("\titem_call failed with code: ");
      msg.Len = sprintf(msg.Data,"%i\n", pa); User_Common_SendData(msg);
      goto bail;
      //return pa;
  }

bail:
  serial_println("\n\n******\nProcess Packet Complete \n*****************\n");
  return pa;
}



void PP_ItemsPrintHelp(void *p) {
	/*
	for (byte i = 0; i < iCurrentItem; i++) {
	p->print(F("Id: "));
	p->println(iItems[i]->Id);
	p->print(F("\tHelpText: "));
	p->println(iItems[i]->HelpText);
	p->print(F("\tSchema: "));
	p->println(iItems[i]->Schema);
	p->print(F("\tDescription: "));
	p->println(iItems[i]->Description);
	}
	*/
}

#ifdef tester
void PP_Unit_Test(){
	  //create a Items
	  ParseItem ItemOne;
	  strcpy(ItemOne.Id,"AA");
	  ItemOne.CallOnMatch = &functionToCall;
	  ItemOne.HelpText=F("This is item one help text");

	  ParseItem ItemTwo;
	  strcpy(ItemTwo.Id,"AB");
	  ItemTwo.CallOnMatch = &testCalledFunction;
	  ItemTwo.HelpText=F("This is item two help text");

	  ParseItem ItemThree((char*)"DF", &functionCalled);
	  ItemThree.Description=F("Itme threee description");
	  ItemThree.Schema=F("Item Three Schema {SNMATT1234:II1234:AA1234}");

	  //create a parser
	  Parser MyParser;
	  Serial.println(F("\n\n*******\nItemAdd: "));
	  MyParser.ItemAdd(&ItemOne);
	  MyParser.ItemAdd(&ItemTwo);
	  MyParser.ItemAdd(&ItemThree);

	  //Use the parser
	  Serial.println(F("\n\n*******\nItemPrintServer: "));
	  MyParser.ItemsPrintServer(&Serial);

	  Serial.println(F("\n\n*******\nItemPrintHelp: "));
	  MyParser.ItemsPrintHelp(&Serial);
	  PaRESULT res;

	  SerialPause();

	  //not long enough
	  Serial.print(F("\n\n----------\nProcessPacket: "));
	  char packet[50] = "S";
	  Serial.print(packet);
	  res=MyParser.ProcessPacket(packet, strlen(packet));
	  Serial.print(F("result :"));
	  Serial.println(res);
	  Serial.println(F("----------"));

	  SerialPause();

	  //long enough but no SN
	  Serial.print(F("\n\n----------\nProcessPacket: "));
	  strcpy(packet,"SMAAAAAAAAA");
	  Serial.print(packet);
	  res=MyParser.ProcessPacket(packet, strlen(packet));
	  Serial.print(F("result :"));
	  Serial.println(res);
	  Serial.println(F("----------"));

	  SerialPause();

	  //wrong SN
	  Serial.print(F("\n\n----------\nProcessPacket: "));
	  strcpy(packet,"SNATT1234:II1234:AA1234");
	  Serial.print(packet);
	  res=MyParser.ProcessPacket(packet, strlen(packet));
	  Serial.print(F("result :"));
	  Serial.println(res);
	  Serial.println(F("----------"));

	  SerialPause();

	  //no II
	  Serial.print(F("\n\n----------\nProcessPacket: "));
	  strcpy(packet,"SNMATT1234:IA1234:AA1234");
	  Serial.print(packet);
	  res=MyParser.ProcessPacket(packet, strlen(packet));
	  Serial.print(F("result :"));
	  Serial.println(res);
	  Serial.println(F("----------"));

	  SerialPause();

	  //no ii
	  Serial.print(F("\n\n----------\nProcessPacket: "));
	  strcpy(packet,"SNMATT1234:I");
	  Serial.print(packet);
	  res=MyParser.ProcessPacket(packet, strlen(packet));
	  Serial.print(F("result :"));
	  Serial.println(res);
	  Serial.println(F("----------"));

	  SerialPause();

	  //ii but no number
	  Serial.print(F("\n\n----------\nProcessPacket: "));
	  strcpy(packet,"SNMATT1234:II");
	  Serial.print(packet);
	  res=MyParser.ProcessPacket(packet, strlen(packet));
	  Serial.print(F("result :"));
	  Serial.println(res);
	  Serial.println(F("----------"));

	  SerialPause();

	  //no instruction
	  Serial.print(F("\n\n----------\nProcessPacket: "));
	  strcpy(packet,"SNMATT1234:II1234");
	  Serial.print(packet);
	  res=MyParser.ProcessPacket(packet, strlen(packet));
	  Serial.print(F("result :"));
	  Serial.println(res);
	  Serial.println(F("----------"));

	  SerialPause();

	  //an instruciton but not match af
	  Serial.print(F("\n\n----------\nProcessPacket: "));
	  strcpy(packet,"SNMATT1234:II1234:AF1234,1233");
	  Serial.print(packet);
	  res=MyParser.ProcessPacket(packet, strlen(packet));
	  Serial.print(F("result :"));
	  Serial.println(res);
	  Serial.println(F("----------"));

	  SerialPause();

	  //all good.
	  Serial.print(F("\n\n----------\nProcessPacket: "));
	  strcpy(packet,"SNMATT1234:II1234:AB1234,12322,JOB_DONE");
	  Serial.print(packet);
	  res=MyParser.ProcessPacket(packet, strlen(packet));
	  Serial.print(F("result :"));
	  Serial.println(res);
	  Serial.println(F("----------"));

}
#endif

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* PACKETPROCESS_H_ */
